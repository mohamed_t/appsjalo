<?php

use Illuminate\Database\Seeder;
use App\Etat;

class EtatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['cours','livre', 'annule'] as $value) 
        {
        	Etat::create(['libelle' => $value]);
        }
    }
}
