<?php

use Illuminate\Database\Seeder;
use App\Role;

class AddFournisseurFMCGTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'role' => 'fournisseurFMCG'

        ]);
    }
}
