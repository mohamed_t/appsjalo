<?php

use Illuminate\Database\Seeder;
use App\Commande;
use App\Catalogue;
use App\CatalogueCommande;

class CataloguesCommandesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = $this->getFaker();
        //$commandes = Commande::all()->count();
        //$catalogues = Catalogue::all()->count();

        for ($i = 0; $i < 50; $i++)
        {

            CatalogueCommande ::create([
                'catalogue_id' => $faker->numberBetween(Catalogue::all()->first()->id, Catalogue::all()->last()->id),
                'commande_id' => $faker->numberBetween(Commande::all()->first()->id, Commande::all()->last()->id),
                'quantite' => $faker->numberBetween($min = 1, $max = 10)

            ]);
        }
    }

    public function getFaker()
    {
        if (empty($this->faker))
        {
            $faker = Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
        }
        return $this->faker = $faker;
    }
}
