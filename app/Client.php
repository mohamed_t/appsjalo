<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    protected $table = 'dAddress';

    protected $fillable = ['fullname', 'quartier' , 'adresse','phone','quartier_id','livraison' , 'userid','created_at'];


    public $timestamps = false;

    public static function getClientsByBoutiquier($boutiquier_id, $date_deb, $date_fin)
    {

        if($date_deb && $date_fin)
        {

            return DB::table('dAddress as c')->select('c.fullname', 'c.phone',
                 'q.nom as quartier', 'c.id')
                ->join('quartiers as q', 'q.id', '=','c.quartier_id')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.created_at', '>=', $date_deb)
                ->where('c.created_at', '<=', $date_fin)
                ->get();
        }

        else if($date_deb)
        {

            return DB::table('dAddress as c')->select('c.fullname', 'c.phone',
            'q.nom as quartier', 'c.id')
           ->join('quartiers as q', 'q.id', '=','c.quartier_id')
           ->where('c.boutiquier_id', $boutiquier_id)
           ->where('c.created_at', '>=', $date_deb)
           ->get();
        }
        else if ($date_fin)
        {

            return DB::table('dAddress as c')->select('c.fullname', 'c.phone',
            'q.nom as quartier', 'c.id')
           ->join('quartiers as q', 'q.id', '=','c.quartier_id')
           ->where('c.boutiquier_id', $boutiquier_id)
           ->where('c.created_at', '<=', $date_fin)
           ->get();

        }
        else
        {

            return DB::table('dAddress as c')->select('c.fullname', 'c.phone',
                 'q.nom as quartier', 'c.id')
                ->join('quartiers as q', 'q.id', '=','c.quartier_id')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->get();
        }

    }

    public static  function  getClientsByCommercial($commercial)
    {

    }
}
