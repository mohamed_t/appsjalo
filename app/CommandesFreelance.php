<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class CommandesFreelance extends Model
{
    protected $table = 'order_freelance';
    protected $fillable = ['freelance_id', 'commercial_id', 'created_at', 'status' , 'total' , 'client_freelance_id'];
    public $timestamps = false;




    public static function getCommandes($user_id) {
        $commandesQuery = DB::table('order_freelance as c')
                        
                         ->join('client_freelances','client_freelances.id', '=', 'c.client_freelance_id')
                         ->select('c.created_at as date',
                        'c.id', 'c.status' , 'client_freelances.nom as nomDuClient' , 'client_freelances.phone as telDuClient' , 'client_freelances.adresse as adresseDuClient')
                         ->where('c.freelance_id',$user_id)
                         //->where('c.commercial_id', $commercial_id)
                         ->orderBy('c.created_at', 'desc')->get();
                         foreach ($commandesQuery as $commande)
                         {
                             $produits = DB::table('order_product_freelance as cc')->select('ca.name as produit', 'cc.qty', 'ca.price')
                             ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                            // ->join('produits as p', 'p.id', '=', 'ca.produit_id')
                             ->where('cc.order_freelance_id', $commande->id)->get();
                 
                             foreach ($produits as $produit)
                             {
                                 $produit->prixTotal = self::calculTotalPrice($produit->qty, $produit->price);
                             }
                             $commande->produits = $produits;
                 
                         }
        return ['commandes' => $commandesQuery];
    }


    public static function calculTotalPrice($quantite, $prix)
    {
         $prixTotal =  $quantite * $prix;

         return $prixTotal;
    }


}
