<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class CatalogueCommandeFreelance extends Model
{
    protected $table = 'order_product_freelance';
    protected $fillable = ['product_id', 'order_freelance_id', 'qty' , 'total'];
    public $timestamps = false;
}
