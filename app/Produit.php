<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\DB;


class Produit extends Model

{
    use Sluggable;
    protected $table = 'products';
    protected $fillable = ['id','name', 'category_id', 'description'];
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsTo(\App\Categorie::class , 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public static function getProduit($product)
    {
        return DB::table('produits')
            ->select('produits.id', 'produits.libelle', 'categories.libelle as category', 'categories.icone', 'p.description')
            ->where('produits.id','=', $product)
            ->join('categories', 'categories.id', '=', 'produits.categorie_id')
            ->get();
    }

    public static function getListProduitToUser($user_id)
    {
        return DB::table('catalogues as c')->select('c.*', 'p.libelle', 'ca.libelle', 'ca.icone', 'p.description', 'c.prix_achat', 'c.commission_boutiquier')
            ->join('produits as p', 'p.id', '=', 'c.fournisseur_id')
            ->join('categories as ca', 'ca.id', '=','p.categorie_id')
            ->where('c.fournisseur_id', '=', $user_id)
            ->get();
    }

    public static function getAllProduct()
    {
        return DB::table('products as p')->select('p.id', 'p.name as nomProduit', 'c.name as categorie', 'p.image', 'p.description')
            ->join('categories as c', 'c.id', '=','p.category_id')
            ->where('p.type_produit', '=', 'Alimentaire')
            ->get();

    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'libelle'
            ]
        ];
    }

}
