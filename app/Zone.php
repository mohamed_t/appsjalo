<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zones';
    protected $fillable = ['id','nom' , 'frais'];
    public $timestamps = false;

    public static function getZone()
    {
        return self::all();
    }
}
