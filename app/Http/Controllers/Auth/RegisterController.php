<?php

namespace App\Http\Controllers\Auth;

use App\Info;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Quartier;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Carbon\Carbon;
use App\Role;
use \Auth;





class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'client/catalogues';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

   /* public function __construct()
    {
        $this->middleware('guest');
    } */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nom' => 'required|string|max:255',
            'password' => 'required|between:6,12|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
            'phone1' => 'required|int|min:7',
            'prenom' => 'required|string|max:255',
            'quartier' => 'required|int',
            'info_id' => 'required|int'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $data = $request->except('token');

        $validator = $this->validator($data);

        $auth = User::where('phone1', $request->get('phone1'))->get()->first();

        if($auth != null)
        {

            $message = 'cet utiliseur existe déja';
            return  Redirect::to('register')->with(['message' => $message]);
        };

        if ($validator->fails()) {
            return  Redirect::to('register')->withErrors($validator);
        }

        if($request->get('password') != $request->get('password_confirmation'))
        {
            $message = 'password and password confirmation does not match';
            return  Redirect::to('register')->with(['message' => $message]);
        }

        $role_id = Role::where('role', 'client')->first()->id;

        $user = new User();

        $user->phone1 = $request->get('phone1');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->nom = $request->get('nom');
        $user->prenom = $request->get('prenom');
        $user->quartier_id = $request->get('quartier');
        $user->date_inscription = Carbon::now();
        $user->role_id = $role_id;
        $user->info_id = $request->get('info_id');

        if($user->save())
        {
            Auth::login($user);
           return redirect()->route('client_catalogues', ['category' => 'all']);

        }
        else
        {
            flash('user not saved')->error();

        }
    }

    public function index()
    {
        $quartiers = Quartier::getQuartier();
        $infos = Info::all();
        return view('auth/register')->with(['quartiers' => $quartiers, 'infos' => $infos]);
    }
}
