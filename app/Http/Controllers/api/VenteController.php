<?php

namespace App\Http\Controllers\Api;

use App\Etat;
use App\Marchandise;
use App\Vente;
use App\VenteMarchandise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\Commande;
use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VenteController extends Controller
{
    /**
     * @SWG\Post(
     *   path="vente",
     *   summary="create new sale",
     *   operationId="create",
     *   tags={"sales"},
     *   @SWG\Parameter(
     *     name="vente",
     *     in="query",
     *     description="array sale",
     *     required=true,
     *      type="array",
     *      @SWG\Items(
     *             type="integer",
     *             format="int32"
     *         ),
     *         collectionFormat="pipes"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="sale is created"),
     *   @SWG\Response(response=400, description="this marchandise does not exit"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="vente_id not possed")
     * )
     *
     */
    public function create(Request $request)
    {
        $ventes = $request->get('vente');

        if(isset($ventes))
        {
            $client_id = JWTAuth::toUser($request->header('Authorization'))->id;

            $reference = Order::quickRandom(14);

            $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;

            $venteRef = Vente::create(['client_id' => $client_id, 'dAddres_id' => $client_id,'date_vente' => Carbon::now(),
                'reference' => $reference,'type_id' => 2, 'etat_id' => $etat_id]);


            foreach ($ventes as $vente)
            {

                //$vente_id = Vente::where('reference', $venteRef->reference)->first()->id;

                $marchandise_query = Marchandise::where('id',$vente['marchandise_id'])->get()->first();


                if($marchandise_query == null || $marchandise_query == [] )
                {
                    return response()->json(['message' => 'this marchandise does not exit'], 400);
                }
                $marchandise_vente = new VenteMarchandise();
                $marchandise_vente->marchandise_id = $vente['marchandise_id'];
                $marchandise_vente->vente_id = $venteRef->id;
                $marchandise_vente->quantite = $vente['quantite'];

                $marchandise_vente->save();

            }
            return response()->json(['message' => 'sale is created'], 200);

        }
        else
        {
            return response()->json(['message' =>'vente_id not possed'],404);
        }
    }

    /**
     * @SWG\Get(
     *   path="boutiquier/{boutiquier_id}/ventes",
     *   summary="get all sales by shopper",
     *   operationId="getVenteByShopper",
     *   tags={"sales"},
     * @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="path",
     *     description="id by shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getVenteByShopper($boutiquier_id)
    {

        $ventes = DB::table('ventes as v')->select('v.date_vente as date',
            'v.reference', 'v.id', 'v.client_id','v.dAddress_id', 'etats.libelle', 'u.name as nomComplet', 'u.phone as phoneClient')
            ->join('ma_boutique as mb', 'mb.dAddress_id', '=', 'v.dAddress_id')
            ->leftJoin('etats','etats.id', '=', 'v.etat_id')
            ->join('users as u','u.id', '=', 'v.dAddress_id')
            ->where('mb.boutiquier_id', $boutiquier_id)->orderBy('v.date_vente', 'desc')->get();


        foreach ($ventes as $vente)
        {
            $products = DB::table('marchandise_vente as mv')->select('p.name as produit', 'mv.quantite as quantite', 'm.prix')
                ->join('marchandises as m', 'm.id', '=', 'mv.marchandise_id')
                ->join('products as p', 'p.id', '=', 'm.product_id')
                ->where('mv.vente_id', $vente->id)->get();
            foreach ($products as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            }

            $vente->products = $products;
        }

        return response()->json(['ventes' => $ventes], 200);

    }

    public static function calculTotalPrice($quantite, $prix)
    {
        $prixTotal =  $quantite * $prix;

        return $prixTotal;
    }

    /**
     * @SWG\Get(
     *   path="boutiquier/ventes",
     *   summary="liste des ventes du boutiquier",
     *   operationId="getSoldersByShopper",
     *   tags={"sales"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */

    public function getSoldersByShopper(Request $request)
    {
        $boutiquier_id = JWTAuth::toUser($request->header('Authorization'))->id;

        $date_deb = $request->get('date_deb');
        $date_fin = $request->get('date_fin');

        if($date_deb && $date_fin)
        {
            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.fullname as nomComplet', 'cl.phone as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl','cl.id', '=', 'c.dAddress_id')
//                ->orWhere('etats.libelle', 'pay_boutiquier')
//                ->orWhere('etats.libelle', 'pay_jalo')
//                ->orWhere('etats.libelle', 'livre_boutique')
//                ->orWhere('etats.libelle', 'livre_client')
//                ->orwhere('etats.libelle', 'livre')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.created_at', '>=', $date_deb)
                ->where('c.created_at', '<=', $date_fin)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();
        }

        else if($date_deb)
        {

            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.fullname as nomComplet', 'cl.phone as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl','cl.id', '=', 'c.dAddress_id')
//                ->orWhere('etats.libelle', 'pay_boutiquier')
//                ->orWhere('etats.libelle', 'pay_jalo')
//                ->orWhere('etats.libelle', 'livre_boutique')
//                ->orWhere('etats.libelle', 'livre_client')
//                ->orwhere('etats.libelle', 'livre')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.created_at', '>=', $date_deb)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();

        }
        else if ($date_fin)
        {

            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.fullname as nomComplet', 'cl.phone as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl','cl.id', '=', 'c.dAddress_id')
//                ->orWhere('etats.libelle', 'pay_boutiquier')
//                ->orWhere('etats.libelle', 'pay_jalo')
//                ->orWhere('etats.libelle', 'livre_boutique')
//                ->orWhere('etats.libelle', 'livre_client')
//                ->orwhere('etats.libelle', 'livre')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.created_at', '<=', $date_fin)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();

        }
        else{

            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.fullname as nomComplet', 'cl.phone as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl','cl.id', '=', 'c.dAddress_id')
//                ->orWhere('etats.libelle', 'pay_boutiquier')
//                ->orWhere('etats.libelle', 'pay_jalo')
//                ->orWhere('etats.libelle', 'livre_boutique')
//                ->orWhere('etats.libelle', 'livre_client')
//                ->orwhere('etats.libelle', 'livre')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();
        }








//        $ventes = DB::table('commandes as c')->select('c.date_commande as date',
//            'c.reference', 'etats.libelle', 'c.id' , 'cl.nom as nomClient', 'cl.prenom as prenomClient', 'cl.phone1 as
//            phoneClient', 'etats.libelle', 'etats.description', 'c.call')
//            ->leftjoin('etats','etats.id', '=', 'c.etat_id')
//            ->join('clients as cl','cl.id', '=', 'c.client_id')
//            ->orWhere('etats.libelle', 'pay_boutiquier')
//            ->orWhere('etats.libelle', 'pay_jalo')
//            ->orWhere('etats.libelle', 'livre_boutique')
//            ->orWhere('etats.libelle', 'livre_client')
//            ->orwhere('etats.libelle', 'livre')
//            ->where('c.boutiquier_id', $boutiquier_id)
//            ->orderBy('c.date_commande', 'desc')->get();

        if($ventes == [])
        {
            return response()->json(['message' => "this boutiquier doesn't have a orders"],200);
        }

        foreach ($ventes as $vente)
        {
            $products = DB::table('order_product cc')->select('p.name as produit', 'cc.qty', 'ca.price', 'ca.pourcentage_boutiquier')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
               //->join('products as p', 'p.id', '=', 'ca.product_id')
                ->where('cc.order_id', $vente->id)->get();

            foreach ($products as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->qty, $produit->price);
            }
            $vente->products = $products;

        }

        return response()->json(['ventes' => $ventes, 'total' => count($ventes) ], 200);
    }


    /**
     * @SWG\Get(
     *   path="commercial/ventes",
     *   summary="liste des ventes du commercial",
     *   operationId="getSoldersBySaler",
     *   tags={"sales"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getSoldersBySaler(Request $request)
    {
        $commercial_id = JWTAuth::toUser($request->header('Authorization'))->id;


        $date_deb = $request->get('date_deb');
        $date_fin = $request->get('date_fin');

    if($date_deb && $date_fin)
    {
            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.fullname as nomComplet', 'cl.phone as phoneClient',
                'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl','cl.id', '=', 'c.dAddress_id')
                ->join('boutiques','boutiques.boutiquier_id', '=', 'c.boutiquier_id')
                ->where('c.created_at', '>=', $date_deb)
                ->where('c.created_at', '<=', $date_fin)
                ->where('boutiques.commercial_id', $commercial_id)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();
    }

        else if($date_deb)
        {

            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'c.id', 'cl.fullname as nomComplet', 'cl.phone as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl', 'cl.id', '=', 'c.dAddress_id')
                ->join('boutiques', 'boutiques.boutiquier_id', '=', 'c.boutiquier_id')
                ->where('boutiques.commercial_id', $commercial_id)
                ->where('c.created_at', '>=', $date_deb)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();

        }
        else if ($date_fin)
        {

            $ventes = DB::table('orders as c')->select('c.created_at as date',
                'c.reference', 'c.id', 'cl.fullname as nomComplet','cl.phone as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.calle')
                ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
                ->join('dAddress as cl', 'cl.id', '=', 'c.dAddress_id')
                ->join('boutiques', 'boutiques.boutiquier_id', '=', 'c.boutiquier_id')
                ->where('boutiques.commercial_id', $commercial_id)
                ->where('c.created_at', '<=', $date_fin)
                ->where(function($query) {
                    $query->where('etats.libelle', 'pay_boutiquier')
                        ->orWhere('etats.libelle', 'pay_jalo')
                        ->orWhere('etats.libelle', 'livre_client')
                        ->orWhere('etats.libelle', 'livre')
                        ->orWhere('etats.libelle', 'livre_boutique');
                })
                ->orderBy('c.created_at', 'desc')->get();

        }
        else{

        $ventes = DB::table('orders as c')->select('c.created_at as date',
            'c.reference', 'etats.libelle', 'c.id', 'cl.fullname as nomComplet','cl.phone as phoneClient', 'etats.description', 'c.calle')
            ->leftjoin('etats', 'etats.id', '=', 'c.etat_id')
            ->join('dAddress as cl', 'cl.id', '=', 'c.dAddress_id')
            ->join('boutiques', 'boutiques.boutiquier_id', '=', 'c.boutiquier_id')
            ->where('boutiques.commercial_id', $commercial_id)
            ->where(function($query) {
                $query->where('etats.libelle', 'pay_boutiquier')
                    ->orWhere('etats.libelle', 'pay_jalo')
                    ->orWhere('etats.libelle', 'livre_client')
                    ->orWhere('etats.libelle', 'livre')
                    ->orWhere('etats.libelle', 'livre_boutique');
            })
            ->orderBy('c.created_at', 'desc')->get();
        }


        if($ventes == [])
        {
            return response()->json(['message' => "this boutiquier doesn't have a orders"],200);
        }

        foreach ($ventes as $vente)
        {
            $products = DB::table('order_product as cc')->select('p.name as produit', 'cc.qty', 'ca.price',
                 'ca.pourcentage_boutiquier')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                //->join('produits as p', 'p.id', '=', 'ca.produit_id')
                ->where('cc.order_id', $vente->id)->get();

            foreach ($products as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->qty, $produit->price);
            }
            $vente->products = $products;

        }

        return response()->json(['ventes' => $ventes, 'total' => count($ventes) ], 200);
    }

}
