<?php

namespace App\Http\Controllers\Api;

use App\MaBoutique;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Commercial;
use Auth;
use App\Quartier;

class MaBoutiqueController extends Controller
{
    /**
     * @SWG\Post(
     *   path="add/boutiquier/favoris",
     *   summary="add shopper favorite",
     *   operationId="addBoutiquierFavoris",
     *   tags={"MaBoutique"},
     *   @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="query",
     *     description="id shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="adding succes'"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function  addBoutiquierFavoris(Request $request)
    {

        $user = JWTAuth::toUser($request->header('Authorization'));

        MaBoutique::create(['client_id' => $user->id, 'boutiquier_id' => $request->get('boutiquier_id')]);

        return response()->json(['message' =>'adding succes']);

    }

   public function getMyBoutique(Request $request)
    {
        $userAuth = JWTAuth::toUser($request->header('Authorization'));

                
                   

                          $boutiquier = DB::table('boutiques')
                          ->select('boutiques.id','boutiques.nom','boutiques.phone','boutiques.adresse',
                          'boutiques.quartier_id','boutiques.created_at','boutiques.commercial_id','commercials.user_id',
                          'quartiers.nom as nomQuartier')
                          ->join('commercials', 'commercials.id', '=', 'boutiques.commercial_id')
                          ->join('quartiers', 'quartiers.id', '=', 'boutiques.quartier_id')
                          ->where('commercials.user_id', $userAuth->id)
                          ->get();

        return response()->json(['boutiquier' => $boutiquier], 200);
    }
    /**
     * @SWG\Get(
     *   path="client/boutiquier/favoris",
     *   summary="get list shopper favorite by customer",
     *   operationId="getBoutiquierFavoris",
     *   tags={"MaBoutique"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getBoutiquierFavoris(Request $request)
    {
        $client = JWTAuth::toUser($request->header('Authorization'));

        $role_id_boutiquier = Role::where('nom', 'boutiquier')->get()->first()->id;

        $boutiquierFavoris = DB::table('users')
                      ->select('users.name', 'users.quartier_id', 'users.phone', 'users.id')
                      ->join('favories as mb', 'mb.boutiquier_id', '=', 'users.id')
                      ->where('mb.client_id', $client->id)->get()->first();
        
	if($boutiquierFavoris == null) {
	 $autre_boutiquiers = DB::table('users')
                             ->select('users.name', 'users.quartier_id', 'users.phone', 'users.id')
                             ->where('users.quartier_id', $client->quartier_id)
                             ->where('users.role_id', $role_id_boutiquier)
                             ->get();
	return response()->json(['boutiquier_favoris' => $boutiquierFavoris, 'autre_boutiquier' => $autre_boutiquiers], 200);
	}

        $autre_boutiquiers = DB::table('users')
                             ->select('users.name', 'users.quartier_id', 'users.phone', 'users.id')
                             ->where('users.quartier_id', $client->quartier_id)
                             ->where('users.role_id', $role_id_boutiquier)
                             ->where('users.id', '!=' ,$boutiquierFavoris->id)
                             ->get();

        return response()->json(['boutiquier_favoris' => $boutiquierFavoris, 'autre_boutiquier' => $autre_boutiquiers], 200);
    }
}
