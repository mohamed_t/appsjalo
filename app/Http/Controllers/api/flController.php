<?php

namespace App\Http\Controllers\Api;

use App\Etat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Commande;
use App\CatalogueCommandeFreelance;
use App\clientFreelance;
use App\CommandesFreelances;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Notification;
use Psy\Command\Command;

class flController extends Controller
{
    //
    public function commander(Request $request) {
        $catalogues = $request->get('command');
        $total = $request->get('montantTotal');

        if(isset($catalogues))
        {
         

            $freelance_id = JWTAuth::toUser($request->header('Authorization'))->id;  
            
           // $reference = Commande::quickRandom(14);

            
            $commande = CommandesFreelances::create(['created_at' => Carbon::now(),
            'freelance_id' => $freelance_id , 'total' => $total ]);

            
            clientFreelance::create(['nom' => $request->get('client')["nomComplet"] ,
            'quartier' => $request->get('client')["quartier"] , 
            'adresse' => $request->get('client')["adresse"] ,
            'phone' =>  $request->get('client')["telephone"] ,
            'created_at' => Carbon::now(),
            'freelance_id' =>  $freelance_id
            ]);


            foreach ($catalogues as $catalogue)
            {
                $commande_id = CommandesFreelances::where('id', $commande->id)->first()->id;

                $catalogue_query = Catalogue::where('id',$catalogue['catalogue_id']);


                if($catalogue_query == [])
                {
                    return response()->json(['message' => 'catalogue does not exit'], 400);
                }

                $catalogueCommande = new CatalogueCommandeFreelance();

                $catalogueCommande->order_freelance_id = $commande_id;
                $catalogueCommande->product_id = $catalogue['catalogue_id'];
                $catalogueCommande->qty = $catalogue['quantite'];
                $catalogueCommande->save();

            }
           // Notification::sendNotif("new_commande", $client, $boutiquier, $commande->id);

            return response()->json(['message' => 'commande is created'], 200);

            
            

            

        }
        else
        {
            return response()->json(['message' =>'catalogues_id not possed'],504);
        }


        
    }

    public function getCommandes(Request $request) {

        $freelance_id= JWTAuth::toUser($request->header('Authorization'))->id;
        
        $commandes = CommandesFreelances::getCommandes($freelance_id); // liste des commandes pour un freelance

        return response()->json(['commandes' => $commandes], 200);

    }


    public function getClients(Request $request) {
        
        $freelance_id= JWTAuth::toUser($request->header('Authorization'))->id;
        
        $clients = clientFreelance::getClients($freelance_id); // liste des clients pour un freelance

        return response()->json(['clients' => $clients], 200);
         
    }


    public function getClientByPhone($phone) {

        $freelance_id= JWTAuth::toUser($request->header('Authorization'))->id;
        
        $clients = clientFreelance::getClientsByPhone($phone); // liste des clients pour un freelance par le num

        return response()->json(['clients' => $clients], 200);


    }
}