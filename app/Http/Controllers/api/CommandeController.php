<?php

namespace App\Http\Controllers\Api;

use App\Catalogue;
use App\Client;
use App\Etat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Commande;
use App\Freelance;
use App\CatalogueCommande;
use App\CatalogueCommandeFreelance;
use App\clientFreelance;
use App\CommandesFreelance;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Notification;
use Psy\Command\Command;
use App\Boutique;
use App\Commercial;
use Auth;
use App\User;
use App\Product;
use App\Order;

class CommandeController extends Controller
{
    /**
     * @SWG\Get(
     *   path="client/commandes",
     *   summary="orders by customer",
     *   operationId="index",
     *   tags={"orders"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
         $client_id= JWTAuth::toUser($request->header('Authorization'))->id;
      //  $client_id = Client::where('phone' , $phone1)->get()->first()->id;

        $commandes = Commande::getCommandesClient($client_id); // liste des commands pour un client

        return response()->json(['commandes_ventes' => $commandes], 200);
    }

    /**
     * @SWG\Post(
     *   path="commande",
     *   summary="create order",
     *   operationId="create",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="command",
     *     in="query",
     *     description="array orders",
     *     required=true,
     *      type="array",
     *      @SWG\Items(
     *             type="integer",
     *             format="int32"
     *         ),
     *         collectionFormat="pipes"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="commande is created"),
     *   @SWG\Response(response=400, description="catalogue does not exit"),
     *   @SWG\Response(response=504, description="catalogues_id not possed"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
public static function BoutiquierCommande(Request $request)
    {


        $catalogues = $request->get('command');
        $total = $request->get('montantTotal');

        if(isset($catalogues))
        {


            $boutiquier= JWTAuth::toUser($request->header('Authorization'));
            $boutiquier_id = $boutiquier->id;
           // $reference = Commande::quickRandom(14);

	    $client_id = $request->get('client_id');
            $commande = Commande::create(['created_at' => Carbon::now(),
            'user_id' => $client_id , 'total' => $total , 'status' => 0, 'boutiquier_id' => $boutiquier_id, 'client_id'=>$client_id, ]);
          
            $current_client = Client::find($client_id);
            //return response()->json(['commande' =>  $id], 200);
            $current_client->quartier_id = $request->quartier_id;
            $current_client->adresse = $request->adresse;
            $current_client->livraison = $request->frais;
            $current_client->save();
            // Client::update([
            //'fullname' => $request->get('client')["nomComplet"] ,
            //'quartier_id' => $request->get('client')["quartier"] ,
            //'adresse' => $request->get('client')["adresse"] ,
            //'livraison' => $request->get('frais') ,
            //'phone' =>  $request->get('client')["telephone"] ,
            //'created_at' => Carbon::now(),
            //'userid' =>  $request->get('client')["client_id"]
            //]);


            foreach ($catalogues as $catalogue)
            {
                $commande_id = Commande::where('id', $commande->id)->first()->id;

                $catalogue_query = Catalogue::find($catalogue['catalogue_id']);
		

                if($catalogue_query == [])
                {
                    return response()->json(['message' => 'Ce produit existe pas'], 400);
                }
		
		$unit_price = $catalogue_query ->price;
                $catalogueCommande = new CatalogueCommande();

                $catalogueCommande->order_id = $commande_id;
                $catalogueCommande->product_id = $catalogue['catalogue_id'];
                $catalogueCommande->qty = $catalogue['quantite'];
	        $catalogueCommande->total = $unit_price* $catalogue['quantite'];
                $catalogueCommande->save();

            }
           // Notification::sendNotif("new_commande", $client, $boutiquier, $commande->id);

            return response()->json(['commande' =>  $commande], 200);






        }
        else
        {
            return response()->json(['message' =>'catalogues_id not possed'],504);
        }

    }



    public static function create(Request $request)
    {
 
	
        $catalogues = $request->get('command');
        $total = $request->get('montantTotal');
        // $tel = $request->get('client')["telephone"];
        // $idboutiquier = $request->get('client')["idBoutiquier"];
        //$etat_id = Etat::where('libelle', 'cours')->get()->first()->id;

        if(isset($catalogues))
        {
         

            $client = JWTAuth::toUser($request->header('Authorization'));  
            $client_id = $client->id;
           // $reference = Commande::quickRandom(14);

            
            $commande = Commande::create(['created_at' => Carbon::now(),
            'user_id' => $client_id , 'total' => $total , 'status' => 0, ]);

            
            Client::create([
            'fullname' => $request->get('client')["nomComplet"] ,
            'quartier_id' => $request->get('client')["quartier"] , 
            'adresse' => $request->get('client')["adresse"] ,
            'livraison' => $request->get('frais') ,
            'phone' =>  $request->get('client')["telephone"] ,
            'created_at' => Carbon::now(),
            'userid' =>  $request->get('client')["client_id"]
            ]);


          foreach ($catalogues as $catalogue)
            {
                $commande_id = Commande::where('id', $commande->id)->first()->id;

                $catalogue_query = Catalogue::where('id',$catalogue['catalogue_id']);


                if($catalogue_query == [])
                {
                    return response()->json(['message' => 'catalogue does not exit'], 400);
                }

                $catalogueCommande = new CatalogueCommande();

                $catalogueCommande->order_id = $commande_id;
                $catalogueCommande->product_id = $catalogue['catalogue_id'];
                $catalogueCommande->qty = $catalogue['quantite'];
                $catalogueCommande->save();

            }
           // Notification::sendNotif("new_commande", $client, $boutiquier, $commande->id);

            return response()->json(['commande' =>  $commande], 200);

            
            

            

        }
        else
        {
            return response()->json(['message' =>'catalogues_id not possed'],504);
        }

    }
    /**
     * @SWG\Get(
     *   path="commande/{commande_id}/annulee",
     *   summary="annule order",
     *   operationId="annuleeCommande",
        @SWG\Parameter(
     *     name="commande_id",
     *     in="path",
     *     description="id order",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="commande annuler avec succes"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function annuleeCommande($commande_id)
    {
        if(Commande::annleeCommande($commande_id) == 'success')
        {
            return response()->json(['message' => 'commande annuler avec succes'],200);
        }
        else
        {
            return response()->json(['message' => 'erreur commande non annulee'], 400);
        }
    }

    /**
     * @SWG\Get(
     *   path="boutiquier/{boutiquier_id}/commandes",
     *   summary="orders by shopper",
     *   operationId="getallOrderByShopper",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="path",
     *     description="id by shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="operation success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getallOrderByShopper($boutiquier_id, Request $request)
    {

        $date_deb = $request->get('date_deb');
        $date_fin = $request->get('date_fin');

        if($date_deb && $date_fin)
        {


            $commandes = DB::table('commandes as c')->select('c.date_commande as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.nom as nomClient', 'cl.prenom as prenomClient', 'cl.phone1 as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.call')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('clients as cl','cl.id', '=', 'c.client_id')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.date_commande', '>=', $date_deb)
                ->where('c.date_commande', '<=', $date_fin)
                ->orderBy('c.date_commande', 'desc')->get();
        }

        else if($date_deb)
        {

            $commandes = DB::table('commandes as c')->select('c.date_commande as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.nom as nomClient', 'cl.prenom as prenomClient', 'cl.phone1 as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.call')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('clients as cl','cl.id', '=', 'c.client_id')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.date_commande', '>=', $date_deb)
                ->orderBy('c.date_commande', 'desc')->get();

        }
        else if ($date_fin)
        {

            $commandes = DB::table('commandes as c')->select('c.date_commande as date',
                'c.reference', 'etats.libelle', 'c.id' , 'cl.nom as nomClient', 'cl.prenom as prenomClient', 'cl.phone1 as 
            phoneClient', 'etats.libelle', 'etats.description', 'c.call')
                ->leftjoin('etats','etats.id', '=', 'c.etat_id')
                ->join('clients as cl','cl.id', '=', 'c.client_id')
                ->where('c.boutiquier_id', $boutiquier_id)
                ->where('c.date_commande', '<=', $date_fin)
                ->orderBy('c.date_commande', 'desc')->get();

        }
        else{

            $commandes = DB::table('orders as c')->select('c.created_at as date',
                  'c.id' , 'cl.fullname as nomClient',  'cl.phone as 
            phoneClient', 'c.status', 'c.total' )
                ->join('dAddress as cl','cl.id', '=', 'c.client_id')
                ->where('c.boutiquier_id', $boutiquier_id)->orderBy('c.created_at', 'desc')->get();
        }


//        $commandes = DB::table('commandes as c')->select('c.date_commande as date',
//            'c.reference', 'etats.libelle', 'c.id' , 'cl.nom as nomClient', 'cl.prenom as prenomClient', 'cl.phone1 as
//            phoneClient', 'etats.libelle', 'etats.description', 'c.call')
//            ->leftjoin('etats','etats.id', '=', 'c.etat_id')
//            ->join('clients as cl','cl.id', '=', 'c.client_id')
//            ->where('c.boutiquier_id', $boutiquier_id)->orderBy('c.date_commande', 'desc')->get();

        if($commandes == [])
        {
            return response()->json(['message' => "this boutiquier doesn't have a orders"],200);
        }

        foreach ($commandes as $commande)
        {
            $produits = DB::table('order_product as cc')->select('p.name as produit', 'cc.qty as quantite', 'cc.total', 'p.price')
                ->join('products as p', 'p.id', '=', 'cc.product_id')
                ->where('cc.order_id', $commande->id)->get();

            //foreach ($produits as $produit)
            //{
           //     $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            //}
            $commande->produits = $produits;

        }

        return response()->json(['commandes' => $commandes], 200);

    }

    public static function calculTotalPrice($quantite, $prix)
    {
        $prixTotal =  $quantite * $prix;

        return $prixTotal;
    }

    /**
     * @SWG\Patch(
     *   path="commande",
     *   summary="update order",
     *   operationId="update",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="commande_id",
     *     in="query",
     *     description="id order",
     *     required=true,
     *     type="integer"
     *   ),

     * @SWG\Parameter(
     *     name="etat",
     *     in="query",
     *     description="status order",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="etat changed success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=504, description="commande_id or etat is empty")
     * )
     *
     */
    public function update(Request $request)
    {
        $commande_id = $request->get('commande_id');

        $etat= $request->get('etat');

        if($commande_id != null && $etat !=null)
        {
            $commande = Commande::find((int)$commande_id);

            if($commande == [])
            {
                return response()->json([message => "ce commmande n'existe pas"]);
            }

            if($etat == "annule")
            {
                $etat_id = Etat::where('libelle', 'annule')->get()->first()->id;
            }
            elseif($etat == "livre")
            {
                $etat_id = Etat::where('libelle', 'livre')->get()->first()->id;
            }
            elseif($etat == "pay_boutiquier")
            {
                $etat_id = Etat::where('libelle', 'pay_boutiquier')->get()->first()->id;
            }
            elseif($etat == "livre_boutique")
            {
                $etat_id = Etat::where('libelle', 'livre_boutique')->get()->first()->id;
                $commande->date_livraison_boutique = Carbon::now();
            }
            elseif($etat == "livre_client")
            {
                $etat_id = Etat::where('libelle', 'livre_client')->get()->first()->id;
                $commande->date_livraison_client = Carbon::now();
            }
            else
            {
                $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;
            }

//            Commande::find($commande_id)->update(['etat_id' => $etat_id]);
              $commande->etat_id = $etat_id;
              $commande->save();

            return response()->json(['message'=> 'etat changed success'],200);

        }
        else
        {
            return response()->json(['message'=> 'commande_id or etat is empty'],404);
        }

    }

    /**
     * @SWG\Get(
     *   path="commercial/commandes",
     *   summary="get orders assigned to commercial",
     *   operationId="getCommandesBycommercial",
     *   tags={"orders"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="not order")
     * )
     *
     */
    public static function getCommandesBycommercial(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        $date_deb = $request->get('date_deb');
        $date_fin = $request->get('date_fin');

        $commandes = Commande::getCommandeByCommercial($user->id, $date_deb, $date_fin);

        if($commandes == [])
        {
            return response()->json(['message' => "not order"],404);
        }

        foreach ($commandes as $commande)
        {
            $produits = DB::table('catalogue_commande as cc')->select('p.libelle as produit', 'cc.quantite', 'ca.prix', 'ca.pourcentage_boutiquier')
                ->join('catalogues as ca', 'ca.id', '=', 'cc.catalogue_id')
                ->join('produits as p', 'p.id', '=', 'ca.produit_id')
                ->where('cc.commande_id', $commande->id)->get();

            foreach ($produits as $produit)
            {
                $produit->prixTotal = self::calculTotalPrice($produit->quantite, $produit->prix);
            }
            $commande->produits = $produits;

        }

        return response()->json(['commandes' => $commandes], 200);
    }


    public function call($commande_id)
    {
        Commande::find($commande_id)->update(['call' => 1]);
        return response()->json(['message'=> 'commande modifiée avec succés'],200);

    }









    ### Partie freelance  ###############################


    public function commander(Request $request) {
        $catalogues = $request->get('command');
        $total = $request->get('montantTotal');

        if(isset($catalogues))
        {
            $user_id = JWTAuth::toUser($request->header('Authorization'))->id;  
            //$freelance_id = Freelance::where('user_id', $user_id)->first()->id;
            //$commercial_id = Commercial::where('user_id', $user_id)->first()->id;
           // $reference = Commande::quickRandom(14);

           if ($request->get('client')["new"] == true) {
            $client = new clientFreelance;
            $client->nom = $request->get('client')["nomComplet"];
            $client->adresse = $request->get('client')["adresse"];
            $client->phone = $request->get('client')["telephone"];
            $client->created_at = Carbon::now();
            $client->freelance_id = $user_id;
            //$client->commercial_id = $commercial_id;

            if ($client->save()) { 
                $commande = CommandesFreelance::create(['created_at' => Carbon::now(), 'status' => 0,
                'freelance_id' => $user_id , 'total' => $total  , 'client_freelance_id' => $client->id]);

                foreach ($catalogues as $catalogue)
                {
                    $commande_id = CommandesFreelance::where('id', $commande->id)->first()->id;
                    $catalogue_query = Catalogue::where('id',$catalogue['catalogue_id']);
                    if($catalogue_query == [])
                    {
                        return response()->json(['message' => 'catalogue does not exit'], 400);
                    }
                    $catalogueCommande = new CatalogueCommandeFreelance();
    
                    $catalogueCommande->order_freelance_id = $commande_id;
                    $catalogueCommande->product_id = $catalogue['catalogue_id'];
                    $catalogueCommande->qty = $catalogue['quantite'];
                    $catalogueCommande->save();
    
                }
               // Notification::sendNotif("new_commande", $client, $boutiquier, $commande->id);
    
                return response()->json(['message' => 'commande is created'], 200);

            }
           }else {

            $commande = CommandesFreelance::create(['created_at' => Carbon::now(),
                'freelance_id' => $user_id , 'total' => $total  , 'client_freelance_id' => $request->get('client')["id"]]);

                foreach ($catalogues as $catalogue)
                {
                    $commande_id = CommandesFreelance::where('id', $commande->id)->first()->id;
    
                    $catalogue_query = Catalogue::where('id',$catalogue['catalogue_id']);
    
    
                    if($catalogue_query == [])
                    {
                        return response()->json(['message' => 'catalogue does not exit'], 400);
                    }
    
                    $catalogueCommande = new CatalogueCommandeFreelance();
    
                    $catalogueCommande->order_freelance_id = $commande_id;
                    $catalogueCommande->product_id = $catalogue['catalogue_id'];
                    $catalogueCommande->qty = $catalogue['quantite'];
                    $catalogueCommande->save();
    
                }
               // Notification::sendNotif("new_commande", $client, $boutiquier, $commande->id);
                return response()->json(['message' => 'commande is created'], 200);
           }
        }
        else
        {
            return response()->json(['message' =>'catalogues_id not possed'],504);
        }


        
    }

    public function getCommandes(Request $request) {

        $user_id= JWTAuth::toUser($request->header('Authorization'))->id;
	
        //$freelance_id = Freelance::where('user_id', $user_id)->first()->id;
       //$commercial_id = Commercial::where('user_id', $user_id)->first()->id;
        $commandes = CommandesFreelance::getCommandes($user_id); // liste des commandes pour un freelance
        $boutiquier = DB::table('boutiques')
        ->select('boutiques.user_id')
        ->join('commercials', 'commercials.id', '=','boutiques.commercial_id')
        ->where('commercials.user_id', $user_id)
        ->get();
	$orders =[];
        
        foreach($boutiquier as $commande) {
        $orders = DB::table('orders')-> 
	select('orders.id','orders.created_at as date','orders.status','orders.total', 'dAddress.fullname as clientName', 'dAddress.phone as clientPhone', 'adresse as adresseClient')
	 -> join('dAddress', 'dAddress.id', '=', 'orders.client_id')
	 -> where('orders.boutiquier_id', $commande-> user_id)
 	 -> orderBy('orders.id', 'DESC') 
	 -> get();

	$products = [];
  
      foreach($orders as $od) {
        $products = DB::table('order_product')->select('order_product.qty as quantity', 'order_product.total', 'products.name', 'products.price')
                        -> join('products', 'products.id', 'order_product.product_id')
                        -> where('order_product.order_id', $od -> id )
                        -> get();
	$od -> produits = $products;
        	}
}
        return response()->json(['commandes' => $commandes, 'commande_boutique' => $orders], 200);

    }


    public function getClients(Request $request) {
        
        $user_id= JWTAuth::toUser($request->header('Authorization'))->id;
        //$freelance_id = Freelance::where('user_id', $user_id)->first()->id;
        //$commercial_id = Commercial::where('user_id', $user_id)->first()->id;
        $clients = clientFreelance::getClients($user_id); // liste des clients pour un freelance
        
        return response()->json(['clients' => $clients], 200);
         
    }


     public function getClientsByPhone($phone , Request $request) {

        $user_id= JWTAuth::toUser($request->header('Authorization'))->id;
        //$role = JWTAuth::toUser($request->header('Authorization'))->role_id;
      
       //$freelance =  Freelance::where('user_id', $user_id)->first();
       //$commercial_id;
       //$freelance_id;
        //if($freelance = '')
         //{
       //$commercial_id = Commercial::where('user_id', $user_id)->first()->id;
       //}
         
        //else
             //{

             //$freelance_id = Freelance::where('user_id', $user_id)->first()->id;
             //}
        $clients = clientFreelance::getClientsByPhone($phone , $user_id); // liste des clients pour un freelance par le num
  

        return response()->json(['clients' => $clients], 200);


    }
public function commercialshopkeeperorders($boutiquier_id)
    {


        $commandes = DB::table('orders as c')->select(
            'c.reference', 'etats.libelle', 'c.id' , 'cl.name as nomClient', 'cl.phone as phoneClient')
            ->leftjoin('etats','etats.id', '=', 'c.etat_id')
            ->join('users as cl','cl.id', '=', 'c.user_id')
            ->get();

        if($commandes == [])
        {
            return response()->json(['message' => "this boutiquier doesn't have a orders"],200);
        }

        foreach ($commandes as $commande)
        {
            $products = DB::table('order_product as cc')->select('p.name as produit', 'cc.qty', 'ca.price')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                ->join('products as p', 'p.id', '=', 'ca.product_id')
                ->where('cc.order_id', $commande->id)->get();
   
            foreach ($products as $product)
            {
                $product->prixTotal = self::calculTotalPrice($product->qty, $produit->price);
            }
            $commande->products = $products;

        }

        return response()->json(['commandes' => $commandes], 200);

    }

    public static function commercialorders(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        //dd($user);

        $commandes = Order::getCommandeByCommercial($user->id);

        if($commandes == [])
        {
            return response()->json(['message' => "not order"],404);
        }

        foreach ($commandes as $commande)
        {
            $products = DB::table('order_product as cc')->select('p.name as product', 'cc.qty', 'ca.price')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                ->join('products as p', 'p.id', '=', 'ca.product_id')
                ->where('cc.order_id', $commande->id)->get();

            foreach ($products as $product)
            {
                $product->prixTotal = self::calculTotalPrice($produit->qty, $product->price);
            }
            $commande->products = $products;

        }

        return response()->json(['commandes' => $commandes], 200);
    }

    public function commercialOrder(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        $commandes = DB::table('orders as c')->select(
            'c.reference', 'etats.libelle', 'c.id' ,'cl.name as nomClient', 'cl.phone as phoneClient')
            ->leftjoin('etats','etats.id', '=', 'c.etat_id')
            ->join('users as cl','cl.id', '=', 'c.user_id')
            ->where('user_id', $user->id)
            ->get();

        return response()->json(['commandes' => $commandes], 200);
    
    }

   public function VenteBoutiquier(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));
        $commandes = DB::table('orders')->select('orders.*')
        ->where('orders.user_id', $user->id)
        ->where('status', '=', 1) 
        ->sum('orders.total');

        $num_commande = DB::table('orders')->select('orders.*')
        ->where('orders.user_id', $user->id)
        ->where('status', '=', 1)
        ->count();
        
       

        return response()->json(['TotalAmount' => $commandes, 'num_commande' => $num_commande], 200);
    
    }

    public function VenteCommercial(Request $request)
    {
         $user = JWTAuth::toUser($request->header('Authorization'));
        $commandes = DB::table('order_freelance')->select('order_freelance.*')
        ->where('order_freelance.freelance_id', $user->id)
        ->where('status', '=', 1)
        ->sum('order_freelance.total');

        $num_commande = DB::table('order_freelance')->select('order_freelance.*')
        ->where('order_freelance.freelance_id', $user->id)
        ->where('status', '=', 1)
        ->count();



        return response()->json(['TotalAmount' => $commandes, 'num_commande' => $num_commande], 200);



}
}
