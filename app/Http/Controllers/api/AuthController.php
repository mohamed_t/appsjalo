<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class AuthController extends Controller
{

    public function login(Request $request)
    {

        $rules = array(
            'password'   => 'required',
            'phone'   => 'required|min:7'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json(['error' =>$validator->errors()],400);
        }

        $credentials['phone'] = $request->get('phone');
        $credentials['password'] = $request->get('password');

        $user = User::authentificate($credentials);

        if(isset($user['message']) && isset($user['status']))
        {
            return response()->json(['message' => $user['message']], $user['status']);
        }
        else
        {
            return response()->json($user);
        }


    }


    public function register(Request $request){
       /*
       * elle recoit en parametre les infos du user : name , email , condiction , role_id , quartier_id
       * 
       * 
       */ 
        
        $auth = User::where('phone',$request->get('phone'))->get()->first();
        
        if($auth != null)
        {
            return response()->json(['message' => 'user already exist'], 201);
        }

        // Creation de l'utilisateur
        $user = new User;
        $user->name = $request->get('name');
        $user->password =  bcrypt($request->get('password'));
        $user->phone = $request->get('phone');
        $user->condiction = $request->get('condiction');
        $user->role_id = $request->get('role_id');
        $user->quartier_id = $request->get('quartier_id');
        $user->created_at = Carbon::now();

         

         if($user->save())
             {
                error_log('la creation a reussi');

                $client = new Client;
                $client->fullname = $request->get('name');
                $client->phone = $request->get('phone');
                $client->created_at = $user->created_at;
               // $client->boutiquier_id = 259;
                $client->quartier_id = $request->get('quartier_id');
                $client->userid = $user->id;
                
                $client->save();

                $credentials['phone'] = $request->get('phone');
                $credentials['password'] = $request->get('password');


                 $userAuth = User::authentificate($credentials);

                 if(isset($userAuth['message']) && isset($userAuth['status']))
                 {
                    error_log('something wrong');
                     return response()->json(['message' => $userAuth['message']], $userAuth['status']);
                 }
                 else
                 {
                    error_log('inscription');
                     return response()->json($userAuth);
                 }
            }
            else
            {   error_log('quelque chose ne vas pas');
                return response()->json(['message' => 'User not be created'], 403);
            }

	//}
        
    }

    public function getAuthUser(Request $request)
    {
        $userAuth = JWTAuth::toUser($request->header('Authorization'));

        $user = User::getUserDetails($userAuth);

        return response()->json(['user' => $user]);
    }


}



