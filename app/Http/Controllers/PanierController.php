<?php

namespace App\Http\Controllers;
use App\Panier;
use App\Catalogue;
use App\Produit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use \Auth;
use App\Commande;
use App\CatalogueCommande;
use Carbon\Carbon;

class PanierController extends Controller
{

    public function index()
    {
        $produits = DB::table('panier as p')->select('pt.libelle', 'c.prix', 'users.nom','users.prenom', 'users.phone1')
                   ->join('Catalogues as c','c.id', '=', 'p.catalogue_id')
                   ->join('produits as pt', 'p.id', '=', 'c.produit_id')
                   ->join('users', 'users.id', '=', 'c.fournisseur_id')
                   ->get();

        return view('client.catalogues')->with(['produits' => $produits]);
    }

    public function create(Request $request)
    {
        $client_id = $request->client_id;
        $catalogue_id = $request->catalogue_id;
        $quantity = $request->quantity;
    
        Panier::create(['catalogue_id' => $catalogue_id, 'user_id' => $client_id, 'quantity' => $quantity]);

        $nbre_panier = Panier::where('user_id', $client_id)->count();

        return response()->json($nbre_panier);
    }

     public function panier()
    {   
        $produits = DB::table('produits as p')->select('p.libelle', 'c.prix', 'users.nom','users.prenom', 'users.phone1',
            'quartiers.nom')
                    ->join('catalogues as c','c.produit_id', '=', 'p.id')
                    ->join('panier', 'panier.catalogue_id', '=', 'c.id')
                    ->join('users', 'users.id', '=', 'panier.user_id')
                    ->join('quartiers', 'quartiers.id', '=', 'users.quartier_id')
                    ->where('panier.user_id', '=', Auth::user()->id)->get();

        //return view('client.panier')->with(['produits' => $produits]);
    }

    public function remove(Request $request)
    {
        Panier::find((int)$request->get('panier_id'))->delete();

        $nbre = Panier::all()->count();

        return  response()->json($nbre);
    }

    public function update(Request $request)
    {
        $quantityAll = $request->get('panierList');
        $allPanier = Panier::all();

          for($i=0; $i < count($quantityAll)-1; $i++)
          {

              $allPanier[$i]->quantity = (int)$quantityAll[$i];
              $allPanier[$i]->save();

          }
        return response()->json('0k');
    }

    public function getList(Request $request)
    {
        $client_id = (int)$request->get('client_id');

        $panierList = Panier::where('user_id', $client_id)->get();

        $reference = Commande::quickRandom(8);

        $commande = Commande::create(['client_id' => $client_id, 'date_commande' => Carbon::now(),
            'reference' => $reference,'etat_id' => 1]);

        foreach($panierList as $panier)
        {

            CatalogueCommande::create(['commande_id' => $commande->id,
                'catalogue_id' => $panier['catalogue_id'],
                'quantite' => $panier['quantity'] ]);

        }

        Panier::where('user_id', $client_id)->delete();
        $panier = Panier::all()->count();
        return response()->json($panier);
    }

}