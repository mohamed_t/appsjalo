<?php

namespace App\Http\Controllers;
use App\Boutique;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo', 'c.promo_prix')
            ->join('produits as p', 'p.id', '=', 'c.produit_id')
            ->join('users', 'users.id', '=', 'c.fournisseur_id')->orderByRaw('RAND()')
            ->limit(8)->get();
        return view('home')->with(['catalogues' => $catalogues]);
    }

    public function getShoppers()
    {
        $boutiques = DB::table('users as u')->select('u.prenom', 'u.nom', 'u.phone1', 'u.adresse')
        ->join('roles as r', 'r.id', '=', 'u.role_id')->where('r.role', 'boutiquier')
            ->paginate(9);
       return view('boutiques')->with(['boutiques' => $boutiques]);
    }

    public function getcatalogues()
    {
        $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo', 'c.promo_prix')
            ->join('produits as p', 'p.id', '=', 'c.produit_id')
            ->join('users', 'users.id', '=', 'c.fournisseur_id')
            ->join('categories', 'categories.id', '=', 'p.categorie_id')
            ->orderBy('date_ajout', 'DESC')
            ->paginate(8);

        return view('catalogues')->with(['catalogues' => $catalogues]);
    }
}
