<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminVenteController extends Controller
{
    public function getVentes()
    {
        $ventes = DB::table('commandes as c')->select('c.id', 'c.date_commande', 'c.reference', 'client.nom as nomClient',
            'client.prenom as prenomClient', 'client.phone1 as phoneClient', 'bou.nom as nomBoutiquier',
            'bou.prenom as prenomBou', 'bou.phone1 as telBou', 'etats.libelle as etat',
            'commercial.prenom as prenomCom', 'commercial.nom as nomCom','commercial.phone1 as telCom')->
        join('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')->
        join('users as client', 'client.id', '=', 'c.client_id')->
        join('users as commercial', 'commercial.id', '=', 'b.commercial_id')->
        join('users as bou', 'bou.id', '=', 'c.boutiquier_id')->
        join('etats', 'etats.id', '=', 'c.etat_id')->where('etats.libelle', 'livre')
            ->orderBy('date_commande', 'DESC')->paginate(16);

        return view('admin.ventes')->with(['ventes' => $ventes]);
    }
}
