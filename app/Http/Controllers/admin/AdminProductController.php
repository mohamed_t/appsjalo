<?php

namespace App\Http\Controllers\Admin;

use App\Catalogue;
use App\Categorie;
use App\Http\Controllers\ProduitController;
use App\Produit;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AdminProductController extends Controller
{
    public function index(Request $request)
    {
        $category =$request->get('categorie');

        if( $request->get('search') != null)
        {
            $search = $request->get('search');

            $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo', 'c.promo_prix', 'c.numberViews')
                ->join('produits as p', 'p.id', '=', 'c.produit_id')
                ->join('users', 'users.id', '=', 'c.fournisseur_id')
                ->where('p.libelle', 'like', '%' . $search . '%')
                ->orderBy('date_ajout', 'DESC')
                ->paginate(8);
        }

        elseif($category){

            $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo', 'c.promo_prix', 'c.numberViews')
                ->join('produits as p', 'p.id', '=', 'c.produit_id')
                ->join('users', 'users.id', '=', 'c.fournisseur_id')
                ->join('categories', 'categories.id', '=', 'p.categorie_id')
                ->where('p.categorie_id', '=', $category)
                ->orderBy('date_ajout', 'DESC')
                ->paginate(8);

        }

        else {

            $catalogues = DB::table('catalogues as c')->select('p.libelle as produit', 'users.nom', 'users.prenom', 'c.prix', 'c.id', 'c.photo', 'c.promo_prix', 'c.numberViews')
                ->join('produits as p', 'p.id', '=', 'c.produit_id')
                ->join('users', 'users.id', '=', 'c.fournisseur_id')->orderBy('date_ajout', 'DESC')
                ->paginate(8);

        }

        $fournisseurs = User::where('role_id', Role::where('role', 'fournisseur')->first()->id)
                        ->select('id', 'nom', 'prenom')->get();

        $commerciaux = User::where('role_id', Role::where('role', 'commercial')->first()->id)
            ->select('id', 'nom', 'prenom')->get();

        $categories = Categorie::select('id', 'libelle')->where('visible', '=',true)->get();

        return view('admin.produits')->with(['catalogues'=> $catalogues])
                   ->with(['fournisseurs' => $fournisseurs])->with(['commerciaux' => $commerciaux])
                   ->with(['categories' => $categories]);
    }

    public function getProducts($fournisseur_id)
    {
        $produits =  DB::table('catalogues')
                     ->where('fournisseur_id', $fournisseur_id)->get();
         return view('admin.fournisseur_produits');
    }

    public function create(Request $request)
    {
        $categorie_id = Categorie::find((int)$request->get('categorie'))->id;
        $descriptionProduit = $request->get('descriptionProduit');
        $produit = Produit::create(['libelle' => $request->get('nomProduit'), 'categorie_id' => $categorie_id, 'description' => $descriptionProduit]);
        $file_name = 'new_photo';
        $file = $request->file($file_name );
        $newFileName = uniqid().'.'.$file->clientExtension();

        $extension = \File::extension($file_name);

        $file->move(public_path().'/images/photos', $newFileName);

        $current = Carbon::now();


        $image ='images/photos/' . $newFileName;

        $categorie_id_produit_grande_surface = DB::table('categories')->where('slug', '=', 'produit-de-grande-surface')->get()->first()->id;

        if($categorie_id == $categorie_id_produit_grande_surface)
        {
            $commission_boutiquier =  ( ((double)$request->get('prixProduit') - (double)$request->get('prixAchat')) * 10 )/100;
        }
        else
        {
            $commission_boutiquier =  ( ((double)$request->get('prixProduit') - (double)$request->get('prixAchat')) * 15 )/100;
        }



        Catalogue::create(['prix' => (int)$request->get('prixProduit'), 'quantite' => 0,'promo_prix' => 0,'produit_id' => $produit->id,
                        'fournisseur_id' => (int)$request->get('fournisseur'), 'commercial_id' => Auth::user()->id,
                        'pourcentage_jalo' => 0, 'pourcentage_boutiquier' => 0, 'prix_achat' => (int)$request->get('prixAchat'), 'commission_boutiquier' => $commission_boutiquier,
                        'date_ajout' => $current, 'photo' => $image, 'date_fin_validite' => $current->addDays(360),'valide' => false]);

        return  'success';
//            [ 'commission' => $commission_boutiquier, 'categorie_id_produit_grande_surface' =>$categorie_id_produit_grande_surface,
//
//                 'categorie_id' => $categorie_id, 'prix_achat' => $request->get('prixAchat'), 'prixProduit' => (double)$request->get('prixProduit')
//               ];

    }

    public function edit($catalogue_id)
    {
        $fournisseurs = User::where('role_id', Role::where('role', 'fournisseur')->first()->id)
            ->select('id', 'nom', 'prenom')->get();

        $categories = Categorie::orderBy('orderBy', 'ASC')->where('visible', '=',true)->get();

        $catalogue = DB::table('catalogues as c')->where('c.id', (int)$catalogue_id)->select('p.libelle as nomProduit',
            'c.prix', 'u.nom as fournisseur_nom', 'u.prenom as fournisseur_prenom', 'c.pourcentage_boutiquier as pourcentage_boutiquier', 'c.id as catalogue_id',
            'c.pourcentage_jalo as pourcentage_jalo', 'c.fournisseur_id','promo_prix', 'p.description',
            'c.id as catalogue_id', 'c.prix_achat',
            'ca.libelle as categorie','ca.id as categorie_id')
            ->join('produits as p', 'p.id', '=', 'c.produit_id')
            ->join('categories as ca', 'ca.id', '=', 'p.categorie_id')
            ->join('users as u', 'u.id', '=', 'c.fournisseur_id')->first();

        return view('admin/edit-produit')->with(['catalogue' => $catalogue])
            ->with(['fournisseurs' => $fournisseurs])->with(['categories' => $categories]);
    }



    public function destroy()
    {

        Catalogue::find((int)$_POST['catalogue_id'])->delete();
        return  'ok';

    }

    public function show($prdduit_id)
    {
        $catalogue = DB::table('catalogues as c')->where('c.id', (int)$prdduit_id)->select('p.libelle as nomProduit',
            'c.prix', 'u.nom as fournisseur_nom', 'u.prenom as fournisseur_prenom', 'c.pourcentage_boutiquier as pourcentage_boutiquier', 'c.id as catalogue_id',
            'c.pourcentage_jalo as pourcentage_jalo', 'c.fournisseur_id','promo_prix', 'p.description','c.photo')
            ->join('produits as p', 'p.id', '=', 'c.produit_id')
            ->join('users as u', 'u.id', '=', 'c.fournisseur_id')->first();

       return view('admin.showProduit')->with(['catalogue' => $catalogue]);
    }

    public function update(Request $request)
    {
        $catalogue = Catalogue::find($request->get('catalogue_id'));

        $categorie =  $request->get('editCategorie');

        $file_name = 'new_photo';
        $file = $request->file($file_name);

        if($file != null) {
            $newFileName = uniqid() . '.' . $file->clientExtension();

            $extension = \File::extension($file_name);

            $file->move(public_path() . '/images/photos', $newFileName);

            $image = 'images/photos/' . $newFileName;
            $catalogue->photo = $image;
        }


        $produit = Produit::find($catalogue->produit_id);


        $produit->libelle = $request->get('editNom');

        $description =$request->get('editDescription');

        $produit->description = isset($description) ? $request->get('editDescription')  : '';

        if( $categorie != "0")
        {
            $produit->categorie_id = (int)$categorie;
        }

        $produit->save();

        $categorie_id_produit_grande_surface = DB::table('categories')->where('slug', '=', 'produit-de-grande-surface')->get()->first()->id;

        if((int)$categorie == $categorie_id_produit_grande_surface)
        {
            $commission_boutiquier =  ( ((double)$request->get('editPrice') - (double)$request->get('prixAchat')) * 10 )/100;
        }
        else
        {
            $commission_boutiquier =  ( ((double)$request->get('editPrice') - (double)$request->get('prixAchat')) * 15 )/100;
        }


        $catalogue->prix = (int)$request->get('editPrice');
        $catalogue->prix_achat = (int)$request->get('prixAchat');

        $catalogue->commission_boutiquier = $commission_boutiquier;

        $catalogue->promo_prix = $request->get('EditPrixPromo');

        $fournisseur_id = (int)$request->get('editFournisseur');

        if( $fournisseur_id != 0)
        {
            $catalogue->fournisseur_id = (int)$request->get('editFournisseur');
        }

        $catalogue->save();

        return  redirect('/admin/produits');
    }



}
