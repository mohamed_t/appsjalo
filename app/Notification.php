<?php

namespace App;

use FCM;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;

class Notification extends Model
{
    public static function sendNotif($type, $client, $boutiquier, $commande_id)
    {

        $commercial_id =  DB::table('boutiques as b')->select('b.commercial_id')
            ->where('boutiquier_id', $boutiquier->id)->first()->commercial_id;

        $token = DB::table('fcm')->where('user_id', $commercial_id)->first()->token;

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $dataBuilder = new PayloadDataBuilder();

        if($type == "new_number")
        {
            $dataBuilder->addData([ "data" =>
                [   'type' => 'new_number',
                    'shop-keeper' => $boutiquier,
                    'added-client' => $client
                ]
            ]);
        }
        elseif($type == "new_commande")
        {
            $dataBuilder->addData([ "data" =>
                [   'type' => 'new_commande',
                    'client-object' => $client,
                    'shop-keeper-object' => $boutiquier,
                    'commande-id' => $commande_id
                ]
            ]);
        }

        $option = $optionBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, null, $data);
        return $downstreamResponse;
    }
}
