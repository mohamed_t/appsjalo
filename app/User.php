<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
//use Cviebrock\EloquentSluggable\SluggableTrait;
//use Cviebrock\EloquentSluggable\Sluggable;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

class User extends Model  implements Authenticatable
{
    // use Sluggable;
    protected $table = 'users';

    protected $fillable = ['password', 'name','email','phone','condiction','admin','status','remember_token' , 'role_id'];
    public $timestamps = false;

    public static function getUsers($role)
    {
        return DB::table('users')->select('nom','prenom','adresse','phone1')
            ->join('roles', 'roles.id','=', 'users.role_id')
            ->where('roles.role', $role)->get();
    }

    public function produits()
    {
        return $this->hasMany(\App\Produit::class);
    }


    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        // TODO: Implement getAuthIdentifierName() method.
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        // TODO: Implement getAuthIdentifier() method.
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        // TODO: Implement getAuthPassword() method.
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }

    // public function sluggable()
    // {
    //     return [
    //         'slug' => [
    //             'source' => 'prenom'
    //         ]
    //     ];
    // }

    public static function authentificate($credentials)
    {
        try
        {

            if (!$token = JWTAuth::attempt($credentials))
            {
                return (['message' =>'invalid_email_or_password', 'status' => 401]);
            }
        } 
        catch (JWTAuthException $e)
        {
            return (['message' =>'failed_to_create_token', 'status' => 401]);
        }
        
        $userAuth = JWTAuth::toUser($token);

        $user = self::getUserDetails($userAuth);

        return ['token' => $token, 'user' => $user];
    }


    public static function getUserDetails($userAuth)
    {
        $role = Role::getRoleById($userAuth->role_id);

        $quartier = Quartier::getQuartierById($userAuth->quartier_id);

        //$info = Info::getInfoById($userAuth->info_id);

        //$role_id = DB::table('roles')->where('nom','=', 'boutiquier')->first()->id;

        // if($role_id == $userAuth->role_id)
        // {
        //     $type = Boutique::where('boutiquier_id', $userAuth->id)->first()->type;

        // }
        // else
        // {
        //   $type = '';
        // }

        $user = ['id' => $userAuth->id,'fullname' => $userAuth->name, 'email' => $userAuth->email,
            'role' =>$role,
             'quartier' => $userAuth->quartier_id,
            'date_inscription' => $userAuth->created_at,
            'phone' => $userAuth->phone];

        return $user;
    }

    public static function getBoutiquiersByQuartier($user)  // list boutiquier de son quartier
    {
        $role_id_boutiquier = Role::where('nom', 'boutiquier')->get()->first()->id;

        return DB::table('users')->select('users.name' , 'users.phone', 'users.id')
                    ->join('quartiers', 'quartiers.id', '=', 'users.quartier_id')
                    ->where('quartier_id', $user->quartier_id)
                    ->where('role_id', $role_id_boutiquier)->get();

    }

    public static function getProduitsByBoutiquier($boutiquier_id)
    {
      return  DB::table('marchandises as m')->select('p.libelle', 'p.id as produit_id', 'm.id as marchandise_id',
            'c.libelle as categorie', 'm.prix', 'm.image')
            ->join('users as u', 'u. id', '=','m.vendeur_id')
            ->join('produits as p', 'p.id', '=', 'm.produit_id')
            ->join('categories as c', 'c.id', '=', 'p.categorie_id')
            ->where('m.vendeur_id', '=', $boutiquier_id)->get();
    }

    public static function verifyUser($client)
    {
        $query = DB::table('dAddress')->where('phone', $client['phone'])->get();

        if($query->count() == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

}
