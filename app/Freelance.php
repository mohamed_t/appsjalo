<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Freelance extends Model
{
    protected $table = 'freelances';

    protected $fillable = ['id', 'nom','adresse', 'phone' , 'quartier_id','user_id' , 'parametre_id'];

}
