<?php

namespace App\Utilities;
use Illuminate\Support\Facades\DB;

class Utility
{
    public static function generateSlug( $tipTitle, $table){

        $slug = str_slug($tipTitle);

        $slugs = DB::table($table)->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get();

        if ($slugs->count() === 0) {
            return $slug;
        }

        // Get the last matching slug
        $lastSlug = DB::table($table)->count();
        // Strip the number off of the last slug, if any
        $lastSlugNumber = intval(str_replace($slug . '-', '', $lastSlug));
        // Increment/append the counter and return the slug we generated
        return $slug . '-' . ($lastSlugNumber + 1);
    }
}