<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;


class Catalogue extends Model
{
  


    protected $table = 'products';

    protected $fillable = ['id', 'price','tarif','created_at','image', 'name' , 'description',
         'commercial_id', ' category_id', 'fournisseur_id', 'pourcentage_boutiquier', 'pourcentage_jalo', 'promo_prix'];

    public $timestamps = false;

    public static function getAllCatalogue($categorie)
    {

        // if($categorie == 'all')
        // {

        // $catalogues = DB::table('products')
        //     ->join('users', 'users.id', '=', 'products.fournisseur_id')
        //     //->join('produits as p', 'p.id', '=', 'products.produit_id')
        //     ->join('categories as c', 'c.id', '=', 'products.category_id')
        //     ->select('products.*', 'products.name as productLibelle', 'products.description',
        //         'c.name as libelleCategorie', 'users.fullname as nomFournisseur','products.image',
        //          'users.id as fournisseur_id', 'c.image')->orderBy('products.created_at', 'DESC')->paginate(8);
        // }

        // elseif ($categorie == 'promo')
        // {
        //     $catalogues = DB::table('products')
        //     ->join('users', 'users.id', '=', 'products.fournisseur_id')
        //     ->join('categories as c', 'c.id', '=', 'products.category_id')
        //     ->select('products.*', 'products.name as productLibelle', 'products.description',
        //     'c.name as libelleCategorie', 'users.fullname as nomFournisseur','products.image',
        //      'users.id as fournisseur_id', 'c.image')
        //         ->whereNotNull('promo_prix')->orderBy('products.created_at', 'DESC')
        //         ->paginate(8);
        // }
        // else
        
            $catalogues = DB::table('products')
            ->join('users', 'users.id', '=', 'products.fournisseur_id')
            ->join('categories as c', 'c.id', '=', 'products.category_id')
            ->select('products.*', 'products.name as productLibelle', 'products.description',
            'c.name as libelleCategorie', 'users.name as nomFournisseur','products.image',
             'users.id as fournisseur_id')
            ->where('products.category_id', $categorie)->orderBy('products.created_at', 'DESC')->paginate(8);
        

        return $catalogues;
    }

    public static function showcatalogue($catalogue_id)
    {
        return DB::table('products as c')
            ->select('c.id as catalogue_id', 'c.prix', 'c.quantite',
                'c.created_at', 'c.image','c.date_fin_validite', 'c.valide',
                'c.slug as slug_catalogue', 'c.commercial_id', 'c.fournisseur_id',
                'c.produit_id as produit_id', 'p.libelle as libelle_produit', 'p.description',
                'commercial.prenom as prenom_commercial', 'commercial.nom as nom_commercial',
                'commercial.adresse as adresse_commercial', 'commercial.phone1 as phone1_commercial',
                'commercial.avatar as photo_avatar','fournisseur.prenom as prenom_fourniseur',
                'fournisseur.nom as nom_fournisseur', 'fournisseur.adresse as fournisseur_adresse',
                'fournisseur.phone1 as phone1_fournisseur', 'fournisseur.avatar as photo_avatar', 'c.commission_boutiquier')
            ->join('produits as p', 'p.id', '=', 'c.produit_id')
            ->join('users as commercial', 'commercial.id', '=', 'c.commercial_id')
            ->join('users as fournisseur', 'fournisseur.id', '=', 'c.fournisseur_id')->where('c.id', '=', $catalogue_id)
            ->get();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
    
   
}
