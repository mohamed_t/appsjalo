<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Categorie extends Model
{
    use Sluggable;
    protected $table = 'categories';
    protected $fillable = [];
    public $timestamps = false;

    public function produits()
    {
        return $this->hasMany(\App\Produit::class);
    }


    public static function getAllCategories()
    {
        //self::where('visible', true)->orderBy('orderBy', 'ASC')->get();
        return self::all();
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug'
            ]
        ];
    }
}
