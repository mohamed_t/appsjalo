<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaBoutique extends Model
{
    protected $table = 'favories';
    protected $fillable = ['id','boutiquier_id', 'client_id','dAddress_id'];
    public $timestamps = false;
}
