<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class clientFreelance extends Model
{
    protected $table = 'client_freelances';

    protected $fillable = ['id', 'nom','adresse', 'phone' , 'quartier_id','freelance_id','commercial_id'];

    public static function  getClients($user_id) {

        $clients = DB::table('client_freelances as cf')
            ->select('id', 'nom','adresse', 'phone' , 'quartier_id')
            ->where('cf.freelance_id', $user_id)
            ->get();


        
            $clients->map(function ($client) {
                $commande = DB::table('order_freelance as c')
                ->join('client_freelances','client_freelances.id', '=', 'c.client_freelance_id')
                ->select('c.created_at as date',
               'c.id', 'c.status' , 'client_freelances.nom as nomDuClient' , 'client_freelances.phone as telDuClient' , 'client_freelances.adresse as adresseDuClient')
                ->where('c.client_freelance_id', $client->id)->orderBy('c.created_at', 'desc')->first();
              
                $produits = DB::table('order_product_freelance as cc')->select('ca.name as produit', 'cc.qty', 'ca.price')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                   // ->join('produits as p', 'p.id', '=', 'ca.produit_id')
                ->where('cc.order_freelance_id', $commande->id)->get();
        
                    foreach ($produits as $produit)
                    {
                        $produit->prixTotal = self::calculTotalPrice($produit->qty, $produit->price);
                    }
                    $commande->produits = $produits;
        
                $client->lastCommand = $commande;
               
            });
            

          return $clients;
    }

    public static function  getClientsByPhone($phone , $user_id) {

        $clients = DB::table('client_freelances as cf')
        ->select('id', 'nom','adresse', 'phone' , 'quartier_id')
        ->where('cf.freelance_id', $user_id)
        ->where('cf.phone' , $phone)
        ->get();

        return $clients;

    }

    public static function calculTotalPrice($quantite, $prix)
    {
         $prixTotal =  $quantite * $prix;

         return $prixTotal;
    }


    
}
