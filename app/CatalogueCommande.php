<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogueCommande extends Model
{
    protected $table = 'order_product';
    protected $fillable = ['product_id', 'order_id', 'qty' , 'total'];
    public $timestamps = false;
    
}
