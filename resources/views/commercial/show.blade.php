<!doctype html>
<html class="no-js" lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jalo</title>
    <link rel="stylesheet" href="/../css/app.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>

@include('layouts.commercial.header-commercial')

<main class="main">
        <p>&nbsp;</p>
        <div class="row">
            <div class="small-12 medium-8 medium-offset-2 large-6 large-offset-3">
                <header class="header-modal">
                    <h4>Détails {{$type}} </h4>
                </header>
                <p>&nbsp;</p>
                <form action="">
                    <div class="row">

                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="{{ ucfirst($user->prenom)}}" disabled="">
                                <label for="last_name" class="active" data-error="..." >Prénom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="{{ucfirst($user->nom)}}" disabled="">
                                <label for="last_name" class="active" data-error="..." >Nom *</label>
                            </div>
                        </div>
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="tel" class="validate" value="@if($user->phone1)  {{$user->phone1}} @else no reseigné @endif" disabled="">
                                <label for="last_name" class="active"  data-error="..." >Téléphone *</label>
                            </div>
                        </div>
                        {{--<div class="small-12 medium-12 large-12">--}}
                            {{--<div class="input-field">--}}
                                {{--<input id="last_name" type="email" class="validate" value="@if($user->email)  {{$user->email}} @else No rensegné @endif" disabled="">--}}
                                {{--<label for="last_name" class="active"  data-error="..." >Email *</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="small-12 medium-12 large-12">
                            <div class="input-field">
                                <input id="last_name" type="text" class="validate" value="@if($user->adresse)  {{$user->adresse}} @else No renseigné @endif" disabled="">
                                <label for="last_name" class="active"  data-error="..." >Adresse </label>
                            </div>
                        </div>
                    </div>
                </form>
                <p>&nbsp;</p>
                {{--<hr>--}}
                <div class="form-footer">
                    <a href="" class="modal-action modal-close waves-effect waves-black btn-flat success button">MODIFIER</a>
                    <a href="" class="modal-action modal-close waves-effect waves-black btn-flat grey button">ANNULER</a>
                </div>
            </div>
        </div>

</main>
<script src="/../js/vendors/jquery.min.js"></script>
<script src="/../js/vendors/awesomplete.min.js"></script>
<script src="/../js/vendors/foundation.min.js"></script>
<script src="/../js/vendors/materialize.min.js"></script>
<script src="/../js/app.js"></script>

</body>
</html>