<div class="row">
        <div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
          <button class="menu-icon" type="button" data-toggle></button>
          <div class="title-bar-title">Menu</div>
        </div>

        <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">
          <div class="top-bar-left">
            <ul class="menu">
              <li><a href="catalogues?category=all" class="is-active waves-effect waves-yellow"><i class="tiny material-icons">view_module</i> <span>Catalogue</span></a></li>
              <li><a href="commandes" class="waves-effect waves-yellow"><i class="tiny material-icons">shopping_cart</i> <span>Commandes</span></a></li>
              <li><a href="boutiques.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shop</i> <span>Boutiques</span></a></li>
              <li><a href="promotions.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shopping_basket</i> <span>Promo</span></a></li>
              <li><a href="fournisseurs.html" class="waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i> <span>Fournisseurs</span></a></li>
            </ul>
          </div>
          <div class="top-bar-right">
            <ul class="menu">
              <li>
                  <a href="#bucket-container" onClick = "showModalProduits()" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span id="card_items_number">{{ $nbr_panier}}</span></a>
              </li>
            </ul>
          </div>
        </div>
      </div>