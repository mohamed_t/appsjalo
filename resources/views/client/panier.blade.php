@if($produitsList != [])
    @for ($i = 0; $i < count($produitsList); $i++)

        <div class="row">
            <div class="small-12 medium-12 large-4 p-r-10">
                <div class="bucket-item">
                    <img src="https://source.unsplash.com/collection/190727" alt="">
                    <strong>{{$produitsList[$i]->libelle}}</strong>
                </div>
            </div>
            <div class="small-12 medium-12 large-2 p-r-10">
                <form action="panier/edit" method = post class="form_quantite">
                    <div class="input-field">
                        <input onclick="" class="quantite-{{$i}}" type="number" value="1" class="validate" oninput="calculTotal()">
                    </div>
                </form>
            </div>
            <div class="small-12 medium-12 large-2 p-r-10"><strong class="price-item">{{$produitsList[$i]->prix}}</strong></div>
            <div class="small-12 medium-12 large-2 tar"><strong class="totalPrice">1600</strong></div>
            <div class="small-12 medium-12 large-2 p-r-10 tar">
                <button class="btn-floating btn-tiny waves-effect waves-black red"
                        id="remove-this-item" onclick="removeItem([{{$produitsList[$i]->panier_id}}, {{$i}}])"><i class="material-icons">close</i></button>
            </div>
        </div>
    @endfor
@endif

<input type="hidden" id="user_id"  value="{{Auth::user()->id}}">
<div class="hidden" id="row-template" style="display: none;">
    <div class="row">
        <div class="small-12 medium-12 large-4 p-r-10">
            <div class="bucket-item">
                <img src="https://source.unsplash.com/collection/190727" alt="">
                <strong class="item-name"></strong>
            </div>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10">
            <form action="panier/edit" method = post class="form_quantite">
                <div class="input-field">
                    <input type="number" value="1" class="quantity" oninput="calculTotal()">
                </div>
            </form>
        </div>
        <div class="small-12 medium-12 large-2 p-r-10"><strong class="price-item"></strong></div>
        <div class="small-12 medium-12 large-2 tar"><strong class="totalPrice"></strong></div>
        <div class="small-12 medium-12 large-2 p-r-10 tar">
            <button class="btn-floating btn-tiny waves-effect waves-black red"
                        id="remove-this-item" onclick="removeItem()"><i class="material-icons">close</i></button>
        </div>
    </div>
</div>
{{ csrf_field() }}
<script src="../js/vendors/jquery.min.js"></script>
<script type="text/javascript">

    function calculTotal()
    {
        var total = 0;
        $('.bucket-items .row').each(function(i, elem){
            var factor = parseInt($(elem).find('input').eq(0).val()) || 0;
            var item_price = parseInt($(elem).find('.price-item').eq(0).text()) || 0;
            var totalElem = $(elem).find('.totalPrice').eq(0).text(item_price * factor);
            total += item_price * factor;
        });

        $('.total-paid strong').text(total);
    }

    function removeItem(panier)
    {
        alert(panier);
        //console.log(panier[0],panier[1]);
//        console.log(panier);
//        $(document).ready(function()
//        {
//            $.post('panier/remove',
//                {
//                panier_id: panier[0],
//                    "_token": $("input[name=_token]").val()
//            }, function(data)
//            {
//                $('.bucket-items .row').eq(panier[1]).remove();
//            });
//        });

    }

    function order()
    {
        var user_id = $('#user_id').val();
        var array = [];
        $('.bucket-items .row').each(function(i, elem){
            var factor = parseInt($(elem).find('input').eq(0).val());
            array.push(factor);
        });

        $(document).ready(function()
        {
            $.post('panier', {
                panierList: array,
                "_token": $("input[name=_token]").val(),
            }, function (data) {
                //console.log(data);
            });

            $.post('panier/list', {
                client_id: user_id,
                "_token": $("input[name=_token]").val(),
            }, function (data) {
                $('#bucket-container').modal('close')
                $("#card_items_number").text(data);
            });

        });


    }


</script>