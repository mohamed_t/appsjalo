
@extends('layouts.admin.master-admin')

@section('title')
    JALÔ - Tableau de bord
@endsection
@section('app-css')
    <link rel="stylesheet" href="/../css/app.css">
@endsection
@section('content')

    <main class="main">
        <p>&nbsp;</p>
        <!-- Détails des produits -->

        <div class="row">
            <div class="small-12 medium-12 large-8 large-offset-2">
                <div class="card card-single-product">
                    <div class="about-the-author">
                        {{--<h3 class="author-title">Produit</h3>--}}
                        <div class="row">
                            <div class="small-12 medium-6 columns">

                                <div class="main-carousel-product">
                                    <div class="carousel-cell">
                                        <img src="{{url('/')}}/{{$categorie->icone}}" alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="small-12 medium-6 columns">
                                <h4 class="separator-left">{{ucfirst($categorie->libelle)}}</h4>
                                @if($categorie->visible == true)
                                    <p>Visible</p>
                                @else
                                    <p>Non visible</p>
                                @endif
                                <a href="/admin/categories" class="add-bucket-btn waves-effect waves-black"> Annuler </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <!-- Flickity Slider -->
@section('script')
    <script src="/../js/vendors/jquery.min.js"></script>
    <script src="/../js/vendors/foundation.min.js"></script>
    <script src="/../js/vendors/materialize.min.js"></script>
    <script src="/../js/app.js"></script>
    <script src="/../js/vendors/flickity.pkgd.min.js"></script>
@endsection
@endsection


